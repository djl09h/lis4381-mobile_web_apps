# LIS4381 - Mobile Web Application Development

## David Lambert

### Assignment 1 Requirements
*Three Parts*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (chs. 1 and 2)

#### README.md file should include the following items:

* Screnshot of AMPPS Installation, PHP Installation
* Screenshot of running Java Hello
* Screenshot of running Android Studio: My First App
* git commands with short descriptions
* Bitbucket repo link: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes)

#### Git Commands w/ Short Descriptions:

1. git init: Initializes a directory as a new, empty Git repo 
2. git status: Shows the files in the repo that are currently staged, unstaged, and untracked
3. git add: Stage all the changes made to a file or directory for the next commit.
4. git commit: Records all changes to a new snapshot
5. git push: Update remote repo with local repo
6. git pull: Merges the remote repo into the local repo
7. git reset: Removes a file or directory that has been added to the staging area.


#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](images/screenshot_ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](images/screenshot_java.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](images/screenshot_android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/djl09h/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/djl09h/myteamquotes/ "My Team Quotes Tutorial")
