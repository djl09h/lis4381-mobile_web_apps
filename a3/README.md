# LIS4381 - Mobile Web Application Development

## David Lambert

### Assignment 3 Requirements
A pet store owner, who owns a number of pet stores, requests that you develop a Web application whereby he and his team can record, track, and maintain relevant company data, based upon the following business rules:

1. A customer can buy many pets, but each pet, if purchased, is purchased by only one customer.

2. A store has many pets, but each pet is sold by only one store.

Remember: an organization’s business rules are the key to a well-designed database.

For the Pet's R-Us business, it's important to ask the following questions to get a better idea of how the database and Web application should work together:

* Can a customer exist without a pet? Seems reasonable. Yes. (optional)

* Can a pet exist without a customer? Again, yes. (optional)

* Can a pet store not have any pets? It wouldn’t be a pet store. (mandatory)

* Can a pet exist without a pet store? Not in this design. (mandatory)

### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1

2. Screenshot of ERD

3. Links to the following files:
    * a3.mwb
    * a3.sql


### Assignment Screenshots:

*Screenshot of ERD:*

![ERD](img/a3erd.png)


### Links

* [a3.mwb](files/a3.mwb)

* [a3.sql](files/a3.sql)
