# LIS4381 - Mobile Web Application Development

## David Lambert

### Project 2 Requirements



### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1

2. Screenshot of index.php 

3. Screenshot of edit_petstore_process.php

4. Screenshot of RSS Feed


### Assignment Screenshots:

*Screenshot of index.php* 

![SS1](img/ss1.png)

*Screenshot of edit_petstore_process.php*

![SS2](img/ss2.png)

*Screenshot of RSS Feed* 

![SS3](img/ss3.png)

[Link to RSS Feed](http://localhost/lis4381/p2/rssfeed.php)
