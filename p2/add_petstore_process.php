<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

$pst_name_v = $_POST['name'];
$pst_street_v = $_POST['street'];
$pst_city_v = $_POST['city'];
$pst_state_v = $_POST['state'];
$pst_zip_v = $_POST['zipcode'];
$pst_phone_v = $_POST['phone'];
$pst_email_v = $_POST['email'];
$pst_url_v = $_POST['url'];
$pst_ytd_v = $_POST['ytd'];

//exit($pst_name_v);

//name
$pattern='/^[a-zA-Z0-9\-_\s]+$/';
$valid_name = preg_match($pattern, $pst_name_v);
//echo $valid_name;
//exit();

//street
$pattern='/^[a-zA-Z0-9\-_\s]+$/';
$valid_street = preg_match($pattern, $pst_street_v);
//echo $valid_street;
//exit();

//city
$pattern='/^[a-zA-Z0-9\-_\s]+$/';
$valid_city = preg_match($pattern, $pst_city_v);
//echo $valid_city;
//exit();

//state
$pattern='/^[a-zA-Z]{2,2}+$/';
$valid_state = preg_match($pattern, $pst_state_v);
//echo $valid_state;
//exit();

//zip
$pattern='/^\d{5,9}+$/';
$valid_zip = preg_match($pattern, $pst_zip_v);
//echo $valid_zip;
//exit();

//phone
$pattern='/^\d{10}+$/';
$valid_phone = preg_match($pattern, $pst_phone_v);
//echo $valid_phone;
//exit();

//email
$pattern='/^([a-z0-9_\.-]+)@([da-z\.-]+)\.([a-z\.]{2,6})$/';
$valid_email = preg_match($pattern, $pst_email_v);
//echo $valid_email;
//exit();

//url
$pattern='/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
$valid_url = preg_match($pattern, $pst_url_v);
//echo $valid_url;
//exit();

//ytd
$pattern='/^\d{1,8}(?:\.\d{0,2})?$/';
$valid_ytd = preg_match($pattern, $pst_ytd_v);
//echo $valid_ytd;
//exit();

// test if required fields are empty
if (
    empty($pst_name_v) ||
    empty($pst_street_v) ||
    empty($pst_city_v) ||
    empty($pst_state_v) ||
    empty($pst_zip_v) ||
    empty($pst_phone_v) ||
    empty($pst_email_v) ||
    empty($pst_url_v) ||
    !isset($pst_ytd_v)
)
{
    $error = "All fields require data. Check all fields and try again.";
    include('global/error.php');
}

else if ($valid_name === 0)
{
    $error = "Name can only contain letters, numbers, hyphens, or periods.";
    include('global/error.php');
}

else if ($valid_street === 0)
{
    $error = "Street can only contain letters, numbers, hyphens, or periods.";
    include('global/error.php');
}

else if ($valid_city === 0)
{
    $error = "City can only contain letters, numbers, hyphens, or periods.";
    include('global/error.php');
}

else if ($valid_state === 0)
{
    $error = "State must contain two letters.";
    include('global/error.php');
}

else if ($valid_zip === 0)
{
    $error = "Zip code must be 5 or 9 digits.";
    include('global/error.php');
}

else if ($valid_phone === 0)
{
    $error = "Phone number must be exactly 10 digits (including area code).";
    include('global/error.php');
}

else if ($valid_email === 0)
{
    $error = "E-Mail must be in format: name@email.com";
    include('global/error.php');
}

else if ($valid_url === 0)
{
    $error = "URL must be a valid URL.";
    include('global/error.php');
}

else if (!is_numeric($pst_ytd_v) || $pst_ytd_v <= 0)
{
    $error = "YTD Sales can only contain numbers and one decimal point, and must be greater than or equal to zero.";
    include('global/error.php');
}

else
{
    require_once('global/connection.php');

//code to process inserts goes here
$query =
"INSERT INTO store
(str_name, str_street, str_city, str_state, str_zip, str_phone, str_email, str_url)
VALUES
(:pst_name_p, :pst_street_p, :pst_city_p, :pst_state_p, :pst_zip_p, :pst_phone_p, :pst_email_p, :pst_url_p)";

try
{
    $statement = $db->prepare($query);
    $statement->bindParam(':pst_name_p', $pst_name_v);
    $statement->bindParam(':pst_street_p', $pst_street_v);
    $statement->bindParam(':pst_city_p', $pst_city_v);
    $statement->bindParam(':pst_state_p', $pst_state_v);
    $statement->bindParam(':pst_zip_p', $pst_zip_v);
    $statement->bindParam(':pst_phone_p', $pst_phone_v);
    $statement->bindParam(':pst_email_p', $pst_email_v);
    $statement->bindParam(':pst_url_p', $pst_url_v);
    $statement->execute();
    $statement->closeCursor();

    //$last_auto_inc_id = $db->lastInsertId();
    //exit($last_auto_inc_id);
    //include('index.php'); //forwarding is faster, one trip to server
    header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)
}

catch (PDOException $e)
{
    $error = $e->getMessage();
    echo $error;
}
}
?>
