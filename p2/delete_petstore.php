<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

$str_id_v = $_POST['str_id'];
//use for initial test of form inputs
//exit($str_id_v);
//exit(print_r($_POST));

require_once('global/connection.php');

$query =
"DELETE FROM store
WHERE str_id = :str_id_p";

try
{
    $statement = $db->prepare($query);
    $statement->bindParam(':str_id_p', $str_id_v); // bind the parameters
    $row_count = $statement->execute();
    $statement->closeCursor();

    //exit($row_count); //DEBUG

    header('Location: index.php'); // redirect to show updated rows
}
catch (PDOException $e)
{
    $error = $e->getMessage();
    echo $error;    // show the error message if any
}

?>
