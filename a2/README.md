# LIS4381 - Mobile Web Application Development

## David Lambert

### Assignment 2 Requirements

1. Watch the assignment videos.
2. Backward-Engineer the screenshots below, using the videos linked to in the Assignment Instructions.

#### README.md file should include the following items:

* Course title, my name, assignment requirements (as per A1)
* Screenshot of running application's first user interface
* Screenshot of running application's second user interface

#### Assignment Screenshots:

*Screenshot of running application's first user interface*:

![First User Interface](images/screenshot1.png)

*Screenshot of running application's second user interface*:

![Second User Interface](images/screenshot2.png)

