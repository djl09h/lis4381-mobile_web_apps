# LIS4381 - Mobile Web Application Development

## David Lambert

[Assignment 1](a1/ "Assignment 1") - Initial IDE Config and Hello World Examples

* Development Installations (AMPPS, JDK, Android Studio)
* Provide screenshots of installations
* Distributed Version Control with Git and Bitbucket
* Completed BitBucket tutorials
* Provide git command descriptions

[Assignment 2](a2/ "Assignment 2") - Simple Android App

* Create "Healthy Recipes" Android app
* Provide screenshots of completed apps

[Assignment 3](a3/ "Assignment 3") - ERD and SQL Creation

* Create an ERD based upon specified business rules
* Forward-Engineer the ERD to a local SQL database
* Export the ERD to an SQL script (.sql file)
* Provide screenshots of completed ERD
* Provide DB resource links

[Assignment 4](a4/ "Assignment 4") - Bootstrap

* Create a simple web app using Bootstrap.
* Uses Bootstrap Client-Side Validation.

[Assignment 5](a5/ "Assignment 5") - SQL-driven Web App

* Prepared SQL Statements
* jQuery DataTables to view data
* Add user-entered data to SQL database
* Server-Side Validation (PHP and RegEx)

[Project 1](p1/ "Project 1") - Android Business Card App

* Create a launcher icon and display in both activities
* Add background color to both activities
* Add border around image and button
* Add text shadow to button
* Provide screenshots of both activities

[Project 2](p2/ "Project 2") - SQL-driven Web App, Part II

* Prepared SQL Statements
* jQuery DataTables to view data
* Server-Side Validation (PHP and RegEx)
* Add EDIT and DELETE functionality to A5