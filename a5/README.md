# LIS4381 - Mobile Web Application Development

## David Lambert

### Assignment 5 Requirements

Create a SQL-driven web app which uses server-side validation and prepared statements (to help prevent SQL injection attacks). Also displays the user-entered data and allows users to add data.

### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1

2. Screenshots per assignment

3. Link to local lis4381 web app

### Assignment Screenshots:

*Screenshots:*

![Main Page](img/index.png)

![Failed Validation](img/add_petstore_process.png)

### Links:

[Local LIS4381 Web App](http://localhost/lis4381/a5/)
