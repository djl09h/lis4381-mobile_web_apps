Static method or property is always associated with a class<br />



<?php

class Person
{
    private $fname;
    private $lname;
    private $age;

    // parameterized constructor
    public function __construct($fn = "John", $ln = "Doe", $ag = 21)
    {
        $this->fname = $fn;
        $this->lname = $ln;
        $this->age = $ag;

        echo("Creating Person object from parameterized constructor (accepts three arguments) <br />");
    }

    public function __destruct()
    {
        echo("Destroying Person object <br />");
    }

    // mutators / setters
    public function SetFname($fn = "Jane")
    {
        $this->fname = $fn;
    }
    //...other setters

    // accessors / getters
    public function GetFname()
    {
        return $this->fname;
    }
    //...other getters
    
}
?>
