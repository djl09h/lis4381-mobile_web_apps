# LIS4381 - Mobile Web Application Development

## David Lambert

### Assignment 4 Requirements

Create a simple web app using Bootstrap. Uses Bootstrap Client-Side Validation. 

### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1

2. Screenshots per assignment

3. Link to local lis4381 web app

### Assignment Screenshots:

*Screenshots:*

![Main Page](img/screen1.png)

![Failed Validation](img/screen2.png)

![Successful Validation](img/screen3.png)

### Links:

[Local LIS4381 Web App](http://localhost/lis4381/)
