# LIS4381 - Mobile Web Application Development

## David Lambert

### Project 1 Requirements

Backward-engineer the screenshots in the Assignment PDF.

1. Create a launcher icon image and display it in both activities (screens)

2. Must add background color(s) to both activities

3. Must add border around image and button

4. Must add text shadow (button)


### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1

2. Screenshot of running application’s first user interface

3. Screenshot of running application’s second user interface


### Assignment Screenshots:

*Screenshot of running application’s first user interface*

![ERD](img/buscard1.png)

*Screenshot of running application’s first user interface*

![ERD](img/buscard2.png)
