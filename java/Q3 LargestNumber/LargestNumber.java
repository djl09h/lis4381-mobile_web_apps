import java.util.Scanner;
public class LargestNumber
{
	public static void main(String[] args) { 
        	int integer1 = 0;
		int integer2 = 0;

		Scanner sc = new Scanner(System.in); 
		
		System.out.println("Program evaluates largest of two integers."); 
		System.out.println("Program does *not* check for non-numeric characters or non-integer values.");  
		
		System.out.print("Enter first integer: "); 
		integer1 = sc.nextInt();  
		System.out.print("Enter second integer: "); 
		integer2 = sc.nextInt();  
		
		System.out.print("\n");  
		
		if (integer1 > integer2) 
		{ 
			System.out.println(integer1 + " is larger than " + integer2); 
		} 
		
		else if (integer2 > integer1) 
		{
		    System.out.println(integer2 + " is larger than " + integer1); 
		}
	 	
		else
	 	{
		    System.out.println(integer1 + " is equal to " + integer2);
		}

		System.out.print("\n");
	}
}
