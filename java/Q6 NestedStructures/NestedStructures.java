// NestedStructures.java (Q6)
// by David J Lambert
// LIS4381
import java.util.Scanner;

public class NestedStructures
{
    public static void main(String[] args)
    {
        int arrayInt[] = { 3, 2, 4, 99, -1, -5, 3, 7 };

        System.out.print("Create an array with the following values: ");
        for (int i=0; i < arrayInt.length; i++) { 
            System.out.print(arrayInt[i]);
            System.out.print(" ");
        }

        System.out.println();

        System.out.println("Array length: " + arrayInt.length);

        Scanner sc = new Scanner(System.in);
        System.out.print("\nEnter search value: ");

        int query = sc.nextInt();
        
        for (int i=0; i < arrayInt.length; i++) {
            if (query == arrayInt[i])
            {
                System.out.println(query + " found at index " + i);
            }
            else
            {
                System.out.println(query + " *not* found at index " + i);
            }
        }

    }

}
