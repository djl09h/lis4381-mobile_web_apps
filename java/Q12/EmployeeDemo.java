// EmployeeDemo.java
// David J Lambert
// 2016-07-12
// LIS4381
import java.util.Scanner;

public class EmployeeDemo
{
    public static void main(String[] args) {
        System.out.println("\n***** Base class default constructor values *****");
        Person p1 = new Person();         // new Person object
        System.out.println("Fname = " + p1.GetFname());
        System.out.println("Lname = " + p1.GetLname());
        System.out.println("Age = " + p1.GetAge());

        Scanner sc = new Scanner(System.in);    // new Scanner object
        // some temporary variables for inputted data
        //String tmpFname, tmpLname;
        //int tmpAge;

        // get input from user
        System.out.println("\n***** Base class user-entered constructor values *****");
        System.out.print("First Name: ");
        String tmpFname = sc.nextLine();
        System.out.print("Last Name: ");
        String tmpLname = sc.nextLine();
        System.out.print("Age: ");
        int tmpAge = sc.nextInt();

        Person p2 = new Person(tmpFname, tmpLname, tmpAge); // new Person object
        System.out.println("Fname = " + p2.GetFname());
        System.out.println("Lname = " + p2.GetLname());
        System.out.println("Age = " + p2.GetAge());

        System.out.println("\n***** Now using setter methods to pass literal values, then print() to display values *****");
        p2.SetFname("Bob");
        p2.SetLname("Wilson");
        p2.SetAge(42);
        p2.print();

        System.out.println("\n\n***** Derived class default constructor values *****");
        Employee e1 = new Employee();
        System.out.println("Fname = " + e1.GetFname());
        System.out.println("Lname = " + e1.GetLname());
        System.out.println("Age = " + e1.GetAge());
        System.out.println("SSN = " + e1.GetSSN());
        System.out.println("Gender = " + e1.GetGender());
        System.out.println("\nOr...");
        e1.print();

        // get input from user
        System.out.println("\n***** Derived class user-entered constructor values *****");
        System.out.print("First Name: ");
        tmpFname = sc.next();
        System.out.print("Last Name: ");
        tmpLname = sc.next();
        System.out.print("Age: ");
        tmpAge = sc.nextInt();
        System.out.print("SSN: ");
        int tmpSSN = sc.nextInt();
        System.out.print("Gender: ");
        char tmpGender = sc.next().charAt(0);

        Employee e2 = new Employee(tmpFname, tmpLname, tmpAge, tmpSSN, tmpGender);
        System.out.println("Fname = " + e2.GetFname());
        System.out.println("Lname = " + e2.GetLname());
        System.out.println("Age = " + e2.GetAge());
        System.out.println("SSN = " + e2.GetSSN());
        System.out.println("Gender = " + e2.GetGender());
        System.out.println("\nOr...");
        e2.print();
        System.out.println();
        
    }
}
