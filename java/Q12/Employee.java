// Employee.java
// David J Lambert
// 2016-07-12
// LIS4381

public class Employee extends Person
{
    // private member data
    private int ssn;
    private char gender;

    // default constructor
    public Employee(){
        ssn = 123456789;
        gender = 'X';
        System.out.println("\nEmployee default constructor called!");
    }

    // parameterized constructor
    public Employee(String newFname, String newLname, int newAge, int newSsn, char newGender){
        super(newFname, newLname, newAge);
        ssn = newSsn;
        gender = newGender;
        System.out.println("\nEmployee parameterized constructor called!");
    }

    // Setters
    public void SetSSN(int newSsn){
        ssn = newSsn;
    }

    public void SetGender(char newGender){
        gender = newGender;
    }

    // Getters
    public int GetSSN(){
        return ssn;
    }

    public char GetGender(){
        return gender;
    }

    public void print(){
        super.print();
        System.out.println(", SSN: " + ssn + ", Gender: " + gender);
    }
}
