// Person.java
// David J Lambert
// 2016-07-12
// LIS4381

public class Person
{
    // member data
    private String fname;
    private String lname;
    private int age;

    // default constructor
    public Person(){
        fname = "John";
        lname = "Doe";
        age = 21;
        System.out.println("\nPerson default constructor called!");
    }
    
    // parameterized constructor
    public Person(String newFname, String newLname, int newAge){
        fname = newFname;
        lname = newLname;
        age = newAge;
        System.out.println("\nPerson parameterized constructor called!");
    }

    // Setters
    public void SetFname(String newFname){
        fname = newFname;
    }

    public void SetLname(String newLname){
        lname = newLname;
    }

    public void SetAge(int newAge){
        age = newAge;
    }

    // Getters
    public String GetFname(){
        return fname;
    }

    public String GetLname(){
        return lname;
    }
    
    public int GetAge(){
        return age;
    }

    public void print(){
        System.out.print("\nFname: " + fname + ", Lname: " + lname + ", Age: " + age);
    }
}
