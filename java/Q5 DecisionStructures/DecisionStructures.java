// DecisionStructures.java
// by David J. Lambert
// LIS4381 Jowett (FSU)
// 2016-05-31

import java.util.Scanner;

public class DecisionStructures
{
    public static void main(String[] args)
    {
        System.out.println("Program evaluates user-entered characters.");
        System.out.println("Use the following characters: W or w, C or c, H or h, N or n.");
        System.out.println("Use the following decision structures: if...else, and switch.");
        System.out.println();
        System.out.println("Phone types: W or w (work), C or c (cell), H or h (home), N or n (none).");
        
        Scanner sc = new Scanner(System.in);        // declare new Scanner object
        System.out.print("Enter phone type: ");     // prompt the user
        char c = sc.next().charAt(0);               // get the input from user
        c = Character.toUpperCase(c);               // convert to upper case

        // *********************************************************************
        // if...else decision structure
        System.out.println("\nif...else:");
        if (c == 'W')
        { System.out.println("Phone type: work"); }
        else if (c == 'C')
        { System.out.println("Phone type: cell"); }
        else if (c == 'H')
        { System.out.println("Phone type: home"); }
        else if (c == 'N')
        { System.out.println("Phone type: none"); }
        else
        { System.out.println("Incorrect character entry."); }

        // *********************************************************************
        // switch decision structure
        System.out.println("\nswitch:");
        switch(c)
        {
            case 'W':
            System.out.println("Phone type: work");
            break;
            
            case 'C':
            System.out.println("Phone type: cell");
            break;

            case 'H':
            System.out.println("Phone type: home");
            break;

            case 'N':
            System.out.println("Phone type: none");
            break;

            default:
            System.out.println("Incorrect character entry.");
        }

        System.out.println();
    }
}
