// Methods.java (Q8)
// by David J Lambert
// 2016-06-21
// LIS4381
import java.util.Scanner;
import java.util.Random;

public class Methods
{
    public static void main(String[] args)
    {
        // Intro text
        System.out.println("Program prompts used to enter desired number of psuedorandom-generated integers (min 1).");
        System.out.println("Uses the following loop structures: for, enhanced for, while, do...while.");
        System.out.println("Creates and displays the values using at least one value-returning method, and one void method.\n");

        // Get input from user
        System.out.print("Enter desired number of integers (to be numbered 1 to N): ");
        Scanner sc = new Scanner(System.in);
        int numInts = sc.nextInt();

        if (numInts < 1)
        {
            System.out.println("Integer must be > 0! Bye!");
            return;
        }

        // Delcare array of ints
        int[] intArray = new int[numInts];

        // pass the array to buildArray (pass by reference)
        buildArray(intArray);
        
        // pass the array to doLoops (pass by reference)
        doLoops(intArray);
    }

    // A VOID METHOD
    private static void doLoops(int[] intArray)
    {
        // ENHANCED FOR LOOP
        System.out.println("\nEnhanced for loop:");
        for (int i : intArray)
        {
            System.out.println(i);
        }

        // declare an iterator for the rest of the loops
        int i = 0;

        // FOR LOOP
        System.out.println("\nfor loop:");
        for (i = 0; i < intArray.length; i++)
        {
            System.out.println(intArray[i]);
        }

        // WHILE LOOP
        System.out.println("\nwhile loop:");
        i = 0;
        while (i < intArray.length)
        {
            System.out.println(intArray[i]);
            i++;
        }

        // DO...WHILE LOOP
        System.out.println("\ndo...while loop:");
        i = 0;
        do {
            System.out.println(intArray[i]);
            i++;
        } while (i < intArray.length);
    }

    // ANOTHER VOID METHOD
    private static void buildArray(int[] intArray)
    {
        for(int i=0; i < intArray.length; i++)
        {
            intArray[i] = generateRandomInt();
        }
    }

    // A FRUITFUL METHOD
    private static int generateRandomInt()
    {
        Random rand = new Random();
        
        return rand.nextInt();

    }
}
