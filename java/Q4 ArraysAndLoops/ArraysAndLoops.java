public class ArraysAndLoops
{
	public static void main(String[] args)
	{
		String animals[] = { "dog", "cat", "bird", "fish", "insect" };

		System.out.println("\nfor loop:");
		for (int i=0; i < animals.length; i++)
		{
			System.out.println(animals[i]);
		}

		System.out.println("\nenhanced for loop:");
		for (String j : animals)
		{
			System.out.println(j);
		}

		System.out.println("\nwhile loop:");
		int k = 0;
		while (k < animals.length)
		{
			System.out.println(animals[k]);
			k++;
		}

		System.out.println("\nwhile loop:");
		int l = 0;
		do {
			System.out.println(animals[l]);
			l++;
		} while (l < animals.length);

		System.out.print("\n");
	}
}
